import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Iterator;

class QwertyParser {

    private static File file;
    private static final String CHARSET = "QWERTYUIOP$#*";

    private static final String REGEX_DOLLAR_TOKEN = "(([QWRTYP]*)([EUIO]{0,2}([QWRTYP]{1}|[$]{2}|[*]{2}|[#]{2}))([$]{0}|[$]{2}|[#]{0}|[#]{2}|[*]{0}|[*]{2}))+";

    private static final String REGEX_ONE_ALPHA = "[QWERTYUIOP]";

    private static final String REGEX_HASH_TOKEN = "([QWRTYP])|([EUIO])|(^([QWRTYP][EUIO])+$)|(^([EUIO][QWRTYP])+$)";

    private static final String REGEX_SENTENCE = "([$]*)([$][#])([$]*)([$][#])([$#])*";

    /**
    * Main
    * @params : args[0] -> Path of the file to parse
    */
    public static void main(String args[]) {

        // Check if Command Line Parameter of filename is present
        if(args.length == 0) {
            System.err.println("No files to parse.");
            System.exit(0);
        }

        // Command Line Parameter is present. Check if file exists
        String filename = args[0];
        file = new File(filename);
        if(!file.exists() || !file.isFile()) {
            System.err.println("\"" + filename + "\"" + " is not a valid file.");
            System.exit(0);
        }

        // File exists. Perform lexical analysis on each line
        performLexicalAnalysis();
    }

    /**
    * Perform Lexical Analysis
    */
    private static void performLexicalAnalysis() {
        // Read the input file line by line
        try {
            Scanner scanner = new Scanner(file);
            int line_number = 0;
            String current_line = null;
            while(scanner.hasNext()) {
                current_line = scanner.nextLine().trim();
                line_number++;
                if(current_line.length() != 0) {
                    System.out.printf("\nReading Line #%d : %s\n-----------------------------\n", line_number, current_line);
                    // System.out.println();

                    ArrayList<String> tokens = extractTokens(current_line);
                    System.out.println("Valid Tokens : " + tokens);

                    if(tokens != null) {
                        String sentence_structure = getSentenceStructure(tokens);
                        // System.out.println("DEBUG : sentence_structure = " + sentence_structure
                        if(isSentenceValid(sentence_structure)) {
                            System.out.println("SENTENCE IS VALID");
                        } else {
                            System.out.println("SENTENCE IS INVALID");
                        }
                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
    * Extract Tokens from line
    */
    private static ArrayList<String> extractTokens(String line) {
        ArrayList<String> tokens = new ArrayList<String>();
        String token = null;
        int word_start = -1;
        boolean error = false;
        // Scan line
        for(int index = 0; index < line.length(); index++) {
            char current_char = line.charAt(index);
            if(!isCharInLanguage(current_char)) {
                System.err.printf("Invalid Character \"%c\" at position %d\n", current_char, index + 1);
                error = true;
                continue;
            }

            // System.out.println("Debug : Word Start = " + word_start);

            if(word_start == -1 && (current_char == '$' || current_char == '#')) {
                word_start = index;
            } else if(word_start != -1) {
                if(current_char == '*') {
                    if( (index + 1)  != line.length() && line.charAt(index + 1) == '*') {
                        index++;
                    } else {
                        token = line.substring(word_start, index+1);
                        if(isTokenValid(token)) tokens.add(token);
                        else error = true;
                        word_start = -1;
                    }
                }
            } else {
                System.err.printf("Expecting token at position %d\n", index + 1);
                error = true;
                break;
            }
        }

        if(word_start != -1) {
            System.out.printf("Token started at %d is not closed.\n", word_start);
            error = true;
        }

        if(error)
        return null;
        return tokens;
    }

    /**
    * Check if character is in languages
    */
    private static boolean isCharInLanguage(char ch) {
        return CHARSET.indexOf(ch) != -1;
    }

    /**
    * Check if token is valid according to the rules
    */
    private static boolean isTokenValid(String token) {
        char first = token.charAt(0);
        Pattern r; Matcher m;
        String token_name = token.substring(1, token.length() - 1);

        // Rule 3 : Check if Token contains at least one alphabet
        if(first == '$') {
            r = Pattern.compile(REGEX_ONE_ALPHA);
            m = r.matcher(token_name);
            if(!m.find()) {
                System.err.printf("Token \"%s\" is invalid.\n", token);
                return false;
            }
        }

        // Rule 1 and 2 : Using Regex
        String pattern = (first == '$')?REGEX_DOLLAR_TOKEN:REGEX_HASH_TOKEN;

        // Create a Pattern object
        r = Pattern.compile(pattern);
        m = r.matcher(token_name);

        if (m.matches()) {
            return true;
        }else {
            System.err.printf("Token \"%s\" is invalid.\n", token);
            return false;
        }
    }

    /**
    * Get the sentence structure in the form of the first characters, e.g. "$$##$$"
    */
    private static String getSentenceStructure(ArrayList<String> tokens) {
        try {
            Iterator<String> iterator =  tokens.iterator();
            String sentence_structure = "";
            while(iterator.hasNext())
                sentence_structure += iterator.next().charAt(0);
            return sentence_structure;
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static boolean isSentenceValid(String sentence_structure) {
        Pattern r = Pattern.compile(REGEX_SENTENCE);
        Matcher m = r.matcher(sentence_structure);
        return m.matches();
    }
}
